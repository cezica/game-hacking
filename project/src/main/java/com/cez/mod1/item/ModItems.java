package com.cez.mod1.item;

import com.cez.mod1.Mod1;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tiers;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItems {
    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, Mod1.MOD_ID);

    public static final RegistryObject<Item> OAT = ITEMS.register("oat",
            () -> new Item(new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> CACAO = ITEMS.register("cacao",
            () -> new Item(new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> OATMEAL = ITEMS.register("ovazcez",
            () -> new Item(new Item.Properties().tab(ModTab.MY_TAB).food(ModFoods.OATMEAL)));

    public static final RegistryObject<Item> MOP036 = ITEMS.register("mop036",
            () -> new SwordItem(Tiers.GOLD,2,3f,
                    new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> PAVELA = ITEMS.register("pavela",
            () -> new Item(new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> PAVELA_BOOTS = ITEMS.register("pavela_boots",
            () -> new ArmorItem(ModArmorMaterial.PAVELA, EquipmentSlot.FEET,
                    new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> PAVELA_CHESTPLATE = ITEMS.register("pavela_chestplate",
            () -> new ArmorItem(ModArmorMaterial.PAVELA, EquipmentSlot.CHEST,
                    new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> PAVELA_LEGGINGS = ITEMS.register("pavela_leggings",
            () -> new ArmorItem(ModArmorMaterial.PAVELA, EquipmentSlot.LEGS,
                    new Item.Properties().tab(ModTab.MY_TAB)));

    public static final RegistryObject<Item> PAVELA_HELMET = ITEMS.register("pavela_helmet",
            () -> new ArmorItem(ModArmorMaterial.PAVELA, EquipmentSlot.HEAD,
                    new Item.Properties().tab(ModTab.MY_TAB)));

    public static void register(IEventBus eventBus){
        ITEMS.register(eventBus);
    }
}
