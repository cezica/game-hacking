package com.cez.mod1.item;

import net.minecraft.world.food.FoodProperties;

public class ModFoods {
    public static final FoodProperties OATMEAL = (new FoodProperties.Builder()).
            nutrition(8).saturationMod(0.3F).build();
}
