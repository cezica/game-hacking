package com.cez.mod1.item;

import com.cez.mod1.Mod1;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModTab {
    //tab personalizat
    public static final CreativeModeTab MY_TAB=new CreativeModeTab(Mod1.MOD_ID) {
        @Override
        public ItemStack makeIcon() {
            return ItemStack.EMPTY;
        }
    };
}
